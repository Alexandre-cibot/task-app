import React from 'react';
import styled from 'styled-components';
import { Draggable } from 'react-beautiful-dnd';

const Container = styled.div`
border: ${props => (props.isDragging ? '0' : '1px solid lightgrey;')};
border-radius: 2px;
padding: 8px;
margin-bottom: 8px;
background-color: ${props => (props.isDragging ? 'lightgreen' : 'white')};
box-shadow: ${props => (props.isDragging ? '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);' : '')};

`

function Task({ task, index }) {
  return (
    <Draggable draggableId={task.id} index={index}>
      {
        (provided, snapshot) => (
          <Container
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
            isDragging={snapshot.isDragging}
          >
            {task.content}
          </Container>
        )
      }
    </Draggable>

  )
}

export default Task;