import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { DragDropContext } from 'react-beautiful-dnd';
import styled from 'styled-components';

import initialData from './initial-data';
import Column from './column';

const Main = styled.div`
display: flex;
flex-direction: row;
`

function App() {
  const [allTasks, setAllTasks] = useState(initialData.tasks);
  const [allColumns, setAllColumns] = useState(initialData.columns);
  const [allColumnsOrder, setAllColumnsOrder] = useState(initialData.columnOrder);

  function handleDragEnd(data) {
    const { destination, source, draggableId } = data;
    console.log(data);
    if (!destination) {
      return;
    }
    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return;
    }

    const columnSource = allColumns[source.droppableId];
    const columnDestination = allColumns[destination.droppableId];
    let newTaskIdsSource;
    let newTaskIdsDestination;

    if (columnSource === columnDestination) {
      newTaskIdsSource = [...columnSource.taskIds];

      newTaskIdsSource.splice(source.index, 1);
      newTaskIdsSource.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...columnSource,
        taskIds: newTaskIdsSource
      }

      setAllColumns({
        ...allColumns,
        [newColumn.id]: newColumn
      })
    } else {
      newTaskIdsSource = [...columnSource.taskIds];
      newTaskIdsDestination = [...columnDestination.taskIds];

      // Remove line on source
      newTaskIdsSource.splice(source.index, 1);
      // Add line on destination
      newTaskIdsDestination.splice(destination.index, 0, draggableId);

      const newColumnSource = {
        ...columnSource,
        taskIds: newTaskIdsSource
      };
      const newColumnDestination = {
        ...columnDestination,
        taskIds: newTaskIdsDestination
      };

      setAllColumns({
        ...allColumns,
        [newColumnSource.id]: newColumnSource,
        [newColumnDestination.id]: newColumnDestination
      })
    }

  }

  return (
    <Main>

      <DragDropContext onDragEnd={handleDragEnd}>
        {
          allColumnsOrder.map(columnId => {
            const column = allColumns[columnId];
            const tasks = column.taskIds.map(taskId => allTasks[taskId]);
            return <Column key={column.id} column={column} tasks={tasks} />
          })
        }
      </DragDropContext>
    </Main>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
